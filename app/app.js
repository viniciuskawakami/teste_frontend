var app = angular.module('app', ['ngRoute', 'ui.bootstrap', 'ngMaterial']);

app.config(function($routeProvider, $locationProvider)
{
   $routeProvider

   // para a rota '/', carregaremos o template index.html e o controller 'VehiclesCtrl'
   .when('/', {
      /*templateUrl : 'app/views/car.html',*/
      templateUrl    : 'index.html',
      controller     : 'VehiclesCtrl'
   })

   // caso não seja nenhum desses, redirecione para a rota '/'
   .otherwise ({ redirectTo: '/' });
});