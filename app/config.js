var config_data = {
    'APP_NAME': 'TExTecnologia',
    'APP_VERSION': '0.1',
    'API_URL': 'https://consulta-veiculos.nimble.com.br/v1/'
}
angular.forEach(config_data, function (key, value) {   
    app.constant(value, key);
});

window.serviceUrl = config_data.API_URL;

