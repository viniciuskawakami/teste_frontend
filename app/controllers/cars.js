var app = angular.module('app');
'use strict';

app.filter('startFrom', function () {
	return function(input, start) {
    	start = parseInt(start, 10);
    	return input.slice(start);
  	};
});

app.directive('ngEnter', function () { //a directive to 'enter key press' in elements with the "ng-enter" attribute
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
})

app.run(function(uibPaginationConfig){
   uibPaginationConfig.firstText='<<';
   uibPaginationConfig.previousText='<';
   uibPaginationConfig.nextText='>';
   uibPaginationConfig.lastText='>>';

})

app.controller('VehiclesCtrl', function($rootScope, $filter, $scope, $uibModal, $http, API_URL) {
    $scope.page = {
        title: "Veículos",
        subtitle: ' '
    }

    // load vehicles list
    $scope.vehicles = [];
    $rootScope.size = 1;
    $scope.srchterm = '';
    
    $scope.$watch('srchterm', function(newValue){
		if(newValue.length === 0){
			console.log('LOG->>> filtro Busca por: VAZIO');
			listVehicles();
		} else {
			console.log('LOG->>> filtro Busca por: TEM CONTEUDO');
		}
	});

    $scope.searchVehicles = function() {
    	if($scope.srchterm) {
    		console.log('LOG->>> filtro Busca por: ', $scope.srchterm);
    		if($scope.srchterm === "") {
    			listVehicles();
    		} else {
    			listVehicles('?filters='+$scope.srchterm);
    		}
    	}
    }
    
    listVehicles();
    function listVehicles(srch){
    	var searchFilter = "";
    	if(!srch) {
    		searchFilter = 'veiculos';
    	} else {
    		searchFilter = 'veiculos'+srch;
    	}
    	console.log("LOG->filtro vehicles => ", searchFilter);
        $http.get(API_URL + searchFilter)
        .then(function(response){
            $scope.vehicles = response.data;
            $rootScope.size = response.data.length;
            console.log("LOG->vehicles => ", response.data);
            console.log("LOG->vehicles.length => ", $rootScope.size);

            // pagination controls
            $scope.viewByList = [
		        { id: 5, name: '5'},
		        { id: 10, name: '10'},
		        { id: 20, name: '20'},
		        { id: 30, name: '30'},
		        { id: 50, name: '50'}
		    ]
		    $scope.viewBy       = [];
		    $scope.viewBy.id    = 5
            $scope.itemsPerPage = 5;
		  	$scope.currentPage  = 1; 
		  	$scope.items        = [];
		  	$scope.items        = $scope.vehicles;
		  	$scope.totalItems   = $rootScope.size;
		  	$scope.maxSize      = 5;

		  	$scope.setPage = function (pageNo) {
				$scope.currentPage = pageNo;
			};

			$scope.pageChanged = function() {
				console.log('Page changed to: ' + $scope.currentPage);
			};

			$scope.setItemsPerPage = function(num) {
				$scope.itemsPerPage = num;
				$scope.currentPage = 1; //reset to first paghe
				console.log('itemsPerPage changed to: ' + $scope.itemsPerPage);
			}

			$scope.pageCount = function() {
				return Math.ceil($scope.totalItems/$scope.itemsPerPage);
			};

        })
        .catch(function(err){
            console.log("err=>", err);
        });
    }

    // change color of active div and active link (glyph) on vehicles list
    function changeClass(id) {
    	console.log("LOG->changeClass: ", id);
    	$scope.activeDiv = id;
    	$scope.activeGlyph = id;
    }

    // load the details of vehicle
    $scope.vehicle = [];

    $scope.detailVehicle = function(id) {
    	$http.get(API_URL + "veiculos/" + id)
    	.then(function(dataCarDetail) {
    		$scope.vehicle = dataCarDetail.data;
            console.log("LOG->vehicle(detailVehicle): ", $scope.vehicle);
            changeClass(id);
        });
    }

    $scope.openVehicleForm = function(id) {
		var URL = 'app/views/car.html';

		if (!id) {
			$scope.edit = {};
			URL = 'app/views/car-new.html'
		}
		   
		$scope.vehicleModal = $uibModal.open({
			templateUrl: URL,
			controller: 'VehiclesCtrl',
			scope: $scope,
			resolve: {
			    items: function () {
			    	return angular.copy($scope.edit.vehicle); // deep copy
			    }
			}
		})

		$scope.vehicleModal.result.then(function (items) {
	    	$scope.edit.vehicle = items;
	    }, function () {
	    	console.log('Modal dismissed at: ' + new Date());
	    })
	};

	$scope.closeVehicleForm = function() {
		$scope.vehicleModal.dismiss('cancel');
	}

	$scope.deleteVehicle = function(id) {
		if (!id) {
			swal({
                title: "Ooops!",
                text:  "Você deve selecionar um veículo primeiro antes de excluir!",
                type:  "warning",
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
		} else {
			console.log('LOG-> deleteVehicle confirm (id): ', id);
			swal({
				title: 'Você tem certeza disso?',
				text: "Esta é uma ação que não pode ser desfeita!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'SIM, exclua!',
				cancelButtonText: 'Cancelar'
			}).then(function () {
				console.log('LOG-> deleteVehicle (id): ', id);
				$http.delete(API_URL + "veiculos/" + id).then(function(response) {
		    		swal(
					    'Excluído!',
					    'Veículo excluído com sucesso!',
					    'success'
					)
					$scope.closeVehicleForm();
		        });
			})
		}
	}

	// open add vehicle form
    $scope.addNewVehicle = function() {
    	$scope.openVehicleForm();
    }

    // add a veihicle
    $scope.addVehicle = function() {
		console.log("LOG-> Add vehicle: ", $scope.edit.vehicle);
    	
    	$scope.save = {};
    	$scope.save.vehicle = {};
    	$scope.save.vehicle.veiculo   = $scope.edit.vehicle.veiculo;
		$scope.save.vehicle.marca     = $scope.edit.vehicle.marca;
		$scope.save.vehicle.ano       = $scope.edit.vehicle.ano;
		$scope.save.vehicle.descricao = $scope.edit.vehicle.descricao;
		$scope.save.vehicle.cod_fipe  = $scope.edit.vehicle.cod_fipe ;
		$scope.save.vehicle.vendido   = $scope.edit.vehicle == true ? true : false;

    	$http.post(API_URL + "veiculos", $scope.save.vehicle).then(function(response) {
    		swal({
                title: "Sucesso",
                text:  "Veículo incluído com sucesso!",
                type:  "success",
                showCancelButton: false,
                showLoaderOnConfirm: false
            }, function() {
                $('.modal-backdrop').hide();
                $('.modal-content').hide();
                $('.modal-backdrop').modal('hide');
                $('.modal-content').modal('hide');
            });          
			$scope.closeVehicleForm();
        });
    }

    $scope.editVehicle = function(id) {
    	if (!id) {
    		swal({
                title: "Ooops!",
                text:  "Você deve selecionar um veículo primeiro!",
                type:  "warning",
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
    	} else {
	    	$scope.edit = {};
	    	$scope.edit.vehicle = {};
	    	console.log("LOG->vehicleId(editVehicle): ", id);

	    	$http.get(API_URL + "veiculos/" + id).then(function(response) {
	    		$scope.edit.vehicle._id       = response.data._id;
	    		$scope.edit.vehicle.veiculo   = response.data.veiculo   == "" ? "" : response.data.veiculo;
	    		$scope.edit.vehicle.marca     = response.data.marca     == "" ? "" : response.data.marca;
	    		$scope.edit.vehicle.ano       = response.data.ano       == "" ? "" : response.data.ano;
	    		$scope.edit.vehicle.descricao = response.data.descricao == "" ? "" : response.data.descricao;
	    		$scope.edit.vehicle.cod_fipe  = response.data.cod_fipe  == "" ? "" : response.data.cod_fipe ;
	    		$scope.edit.vehicle.vendido   = response.data.vendido   == false ? false : true;
	    		console.log("LOG->vehicle Edit Detais (editVehicle): ", $scope.edit.vehicle);
	    		$scope.vehicle = $scope.edit.vehicle;
	           	$scope.openVehicleForm(id);
	        });
	    }
    }
    
    // save a veihicle
    $scope.saveVehicle = function(id) {
    	if (!id) {
    		swal({
                title: "Ooops!",
                text:  "Você precisa preencher o formulário primeiro!",
                type:  "warning",
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
    	} else {
	    	console.log("LOG-> Save vehicle: ", $scope.edit.vehicle);
	    	
	    	$scope.save = {};
	    	$scope.save.vehicle = {};
	    	$scope.save.vehicle.veiculo   = $scope.edit.vehicle.veiculo;
			$scope.save.vehicle.marca     = $scope.edit.vehicle.marca;
			$scope.save.vehicle.ano       = $scope.edit.vehicle.ano;
			$scope.save.vehicle.descricao = $scope.edit.vehicle.descricao;
			$scope.save.vehicle.cod_fipe  = $scope.edit.vehicle.cod_fipe ;
			$scope.save.vehicle.vendido   = $scope.edit.vehicle == true ? true : false;

	    	$http.put(API_URL + "veiculos/" + $scope.edit.vehicle._id, $scope.save.vehicle).then(function(response) {
	    		
	    		swal({
	                title: "Sucesso",
	                text:  "Veículo salvo com sucesso!",
	                type:  "success",
	                showCancelButton: false,
	                showLoaderOnConfirm: false
	            }, function() {
	                $('.modal-backdrop').hide();
	                $('.modal-content').hide();
	                $('.modal-backdrop').modal('hide');
	                $('.modal-content').modal('hide');    
	            });          
				$scope.closeVehicleForm($scope.edit.vehicle._id);

	        });
	    }
    }

});
